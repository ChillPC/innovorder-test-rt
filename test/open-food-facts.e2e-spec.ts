import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { OpenFoodFactsModule } from '../src/open-food-facts/open-food-facts.module';
import { INestApplication } from '@nestjs/common';
import { JwtAuthGuard } from '../src/auth/guard/jwt-auth.guard';
import { MockGuard } from './mock-guard';
import { AppModule } from '../src/app.module';

describe('Open Food Facts', () => {
  let app: INestApplication;
  // let catsService = { getOffProduct: () => partialBnRes };

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule, OpenFoodFactsModule],
    }) // .overrideProvider(OpenFoodFactsService).useValue(catsService)
      .overrideProvider(JwtAuthGuard)
      .useClass(MockGuard)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it(`/GET Open Food Facts product`, () => {
    const productId = '11840497';

    return request(app.getHttpServer())
      .get(`/off/${productId}`)
      .expect(200)
      .expect((res) => {
        if (res.body.code !== productId)
          throw new Error('Missing code in off response');
        if (res.body.product._id !== productId)
          throw new Error('Missing product id');
        return true;
      });
  });
});
