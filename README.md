# Innovorder test - Rémi Teissier

Here is my solution for the [innovorder hiring test](https://github.com/InnovOrder/software-technical-tests/tree/master/crud-nestjs).

## Description

Fonctionnalités indispensables :

- [x] permettre l'inscription d'un utilisateur via login / password
- [x] permettre l'authentification d'un utilisateur via login / password
- [x] sur une route authentifiée, permettre la recherche d'un produit par son code barre sur l'API de OpenFoodFacts

Points bonus :

- [x] Permettre la mise à jour de l'utilisateur
- [x] Système de caching des appels à OpenFoodFacts
- [x] Dockerisation
- [ ] Manifest Kubernetes pour déploiement

## Running the app

The project can be entirely run with docker. You need to build the image and then launch the services described in `docker-compose.yml`

```bash
docker build . --tag innov-rt
docker compose up
```

It will run on port `3000`. You can also look at the openApi specification at <localhost:3000/api>.

## How to use it

You can import `./Insomnia_2023-02-21.json` in insomnia to get those requests (don't forget to change the env var `jwt` after login).

You can create your user by calling:

```bash
curl --request POST \
  --url http://localhost:3000/user/create \
  --header 'Content-Type: application/json' \
  --data '{
	"username": "myUser",
	"password": "MyStr0ngPa$$W0rd"
}'
```

You can then log in your new account with:

```bash
curl --request POST \
  --url http://localhost:3000/auth/login \
  --header 'Content-Type: application/json' \
  --data '{
	"username": "myUser",
	"password": "MyStr0ngPa$$W0rd"
}'
```

It will give your `JWT` for the next requests:

```bash
{ "access_token": "<my_jwt>" }
```

You can then get your product data from open food facts:

```bash
curl --request GET \
  --url http://localhost:3000/off/7622210449283 \
  --header 'Authorization: Bearer <my_jwt>'
```

You can also change your credentials with some queries but your **JWT is bound to your username at log time**. It means that if the username changes, you will nedd to log in again using your new username.

You can modify your account by changing only your username, your password or both at the same time:

```bash
curl --request PUT \
  --url http://localhost:3000/user/update \
  --header 'Authorization: Bearer <my_jwt>' \
  --header 'Content-Type: application/json' \
  --data '{
	"newUsername": "myUser2",
	"newPassword": "MySec0ndStr0ngPa$$W0rd"
}'
```

Then log again in your new account to get your new jwt `my_jwt2`:

```bash
curl --request POST \
  --url http://localhost:3000/auth/login \
  --header 'Content-Type: application/json' \
  --data '{
	"newUsername": "myUser2",
	"newPassword": "MySec0ndStr0ngPa$$W0rd"
}'
```

You can also delete your account with (please throw away your jwt then):

```bash
curl --request DELETE \
  --url http://localhost:3000/user/delete \
  --header 'Authorization: Bearer ' \
  --header 'Content-Type: application/json'
```

## How to test

First launch a test environment with just the database:

```bash
docker compose -f docker-compose-test.yml up
```

Then in the workspace of the project, launch:

```bash
export JWT_SECRET=123 && export DB_USERNAME=innov && export DB_PASSWORD=innov && export DB_NAME=innov && export DB_HOST=localhost
npm run test:e2e
```
