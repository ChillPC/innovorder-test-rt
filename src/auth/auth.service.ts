import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { UserService } from '../user/user.service';
import { LoginDto } from './auth.dto';

@Injectable()
export class AuthService {
  constructor(
    @Inject(forwardRef(() => UserService))
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  private static saltOrRounds = 10;

  hashPassword(password: string): Promise<string> {
    return bcrypt.hash(password, AuthService.saltOrRounds);
  }

  validatePassword(passwordHash: string, password: string): Promise<boolean> {
    return bcrypt.compare(password, passwordHash);
  }

  async validateUser(
    username: string,
    password: string,
  ): Promise<LoginDto | undefined> {
    const passwordHash = await this.userService.getUserPasswordHash(username);

    if (!passwordHash) return undefined;

    if (!(await this.validatePassword(passwordHash, password)))
      return undefined;

    return { username, password };
  }

  async login(user: any): Promise<{ access_token: string }> {
    const payload = { username: user.username };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
