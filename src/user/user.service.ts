import {
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthService } from '../auth/auth.service';
import { DataSource, Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './user.entity';

@Injectable()
export class UserService {
  constructor(
    @Inject(forwardRef(() => AuthService))
    private authService: AuthService,

    @InjectRepository(User)
    private userRepository: Repository<User>,
    private dataSource: DataSource,
  ) {}

  // private executeSqlTransation(possibleError?: string) {

  // }

  async deleteUser(username: string) {
    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      await this.dataSource
        .createQueryBuilder(queryRunner)
        .delete()
        .from(User)
        .where('username = :username', { username })
        .execute();

      await queryRunner.commitTransaction();
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw new BadRequestException(
        `User with username "${username}" does not exist`,
      );
    } finally {
      await queryRunner.release();
    }
  }

  async createUser(createUserDto: CreateUserDto) {
    const user = new User();
    user.username = createUserDto.username;
    user.passwordHash = await this.authService.hashPassword(
      createUserDto.password,
    );

    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      await queryRunner.manager.save(user);

      await queryRunner.commitTransaction();
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw new BadRequestException(
        `User with username "${user.username}" already exist`,
      );
    } finally {
      await queryRunner.release();
    }
  }

  async updateUser(username: string, updateUserDto: UpdateUserDto) {
    const user = await this.findOneByUsername(username);

    if (!user)
      throw new NotFoundException(`User with username "${username}" not found`);

    if (!updateUserDto.newUsername && !updateUserDto.newPassword) return;

    if (updateUserDto.newUsername) user.username = updateUserDto.newUsername;

    if (updateUserDto.newPassword) {
      user.passwordHash = await this.authService.hashPassword(
        updateUserDto.newPassword,
      );
    }

    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      await queryRunner.manager.save(user);

      await queryRunner.commitTransaction();
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw new BadRequestException(
        `User with username "${user.username}" already exist`,
      );
    } finally {
      await queryRunner.release();
    }
  }

  findOneByUsername(username: string): Promise<User | undefined> {
    return this.userRepository.findOneBy({
      username: username,
    });
  }

  async getUserPasswordHash(username: string): Promise<string | undefined> {
    const user = await this.userRepository.findOneBy({
      username: username,
    });

    return user?.passwordHash;
  }
}
