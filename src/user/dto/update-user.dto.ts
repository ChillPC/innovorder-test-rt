import { ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsString,
  IsNotEmpty,
  IsStrongPassword,
  IsOptional,
} from 'class-validator';

export class UpdateUserDto {
  @ApiPropertyOptional({
    description: 'New username to use.',
  })
  @IsString()
  @IsNotEmpty()
  @IsOptional()
  newUsername?: string;

  @ApiPropertyOptional({
    description: 'New password to use. Needs to be strong',
  })
  @IsStrongPassword()
  @IsOptional()
  newPassword?: string;
}
