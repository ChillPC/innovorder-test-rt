import {
  Body,
  Controller,
  Delete,
  HttpException,
  HttpStatus,
  Post,
  Put,
  Req,
} from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiHeader } from '@nestjs/swagger';
import { Request } from 'express';
import { Public } from '../auth/auth.decorator';
import { CreateUserDto } from './dto/create-user.dto';
import { UserDto } from './dto/jwt-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Post('create')
  @Public()
  @ApiCreatedResponse({
    description: 'Create a user.',
  })
  async create(@Body() createUserDto: CreateUserDto) {
    try {
      await this.userService.createUser(createUserDto);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }

  @Delete('delete')
  @ApiCreatedResponse({
    description: 'Delete the user currently logged in.',
  })
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer <your_jwt>',
  })
  @ApiBearerAuth()
  async delete(@Req() request: Request) {
    const user = request.user as UserDto;

    try {
      await this.userService.deleteUser(user.username);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }

  @Put('update')
  @ApiCreatedResponse({
    description: 'Update the user currently logged in.',
  })
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer <your_jwt>',
  })
  @ApiBearerAuth('Authorization')
  async update(@Req() request: Request, @Body() updateUserDto: UpdateUserDto) {
    const user = request.user as { username: string };

    try {
      await this.userService.updateUser(user.username, updateUserDto);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
}
