export default () => ({
  port: parseInt(process.env.PORT, 10) || 3000,
  database: {
    host: process.env.DB_HOST || 'localhost',
    port: parseInt(process.env.DB_PORT, 10) || 3306,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
  },
  cache: {
    ttl: parseInt(process.env.CACHE_TTL, 10) || 30 * 60 * 1000,
    max: parseInt(process.env.CACHE_MAX, 10) || 100,
  },
  jwtSecret: process.env.JWT_SECRET,
});
