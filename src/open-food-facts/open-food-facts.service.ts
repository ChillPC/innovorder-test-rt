import { HttpService } from '@nestjs/axios';
import { Cache } from 'cache-manager';
import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';

@Injectable()
export class OpenFoodFactsService {
  constructor(
    @Inject(CACHE_MANAGER) private cache: Cache,
    private http: HttpService,
  ) {}

  async getOffProduct(id: string): Promise<any> {
    const cacheData = await this.cache.get(id);

    if (cacheData) return cacheData;

    const res = await this.http.axiosRef.get(
      `https://world.openfoodfacts.org/api/v0/product/${id}.json`,
    );

    this.cache.set(id, res.data);

    return res.data;
  }
}
