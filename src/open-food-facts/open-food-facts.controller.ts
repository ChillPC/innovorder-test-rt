import { Controller, Get, Param } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiHeader } from '@nestjs/swagger';
import { OpenFoodFactsService } from './open-food-facts.service';

@Controller('off')
export class OpenFoodFactsController {
  constructor(private openFoodFactsService: OpenFoodFactsService) {}

  @Get(':id')
  @ApiBearerAuth('jwt-token')
  @ApiCreatedResponse({
    description: 'Request open food facts product information',
  })
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer <your_jwt>',
  })
  async getOffProduct(@Param('id') id: string) {
    return this.openFoodFactsService.getOffProduct(id);
  }
}
